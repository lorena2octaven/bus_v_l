from newbus import Bus

from basededatos import BaseDeDatos




class AdminBus:
    listabus=[]

    def AddBus(newbus):
        
        AdminBus.listabus.append(newbus)
        idd= newbus.GetIdd()
        Nombre= newbus.GetNombre()
        plazas= int(newbus.GetNumeroPlazas())
        libres= int(newbus.GetNumeroLibres())

        insert=("INSERT INTO BUS(Idbus, Nombre, Numero_plazas, Numero_libres) VALUES('%i','%s','%i','%i')" %(idd, Nombre,plazas,libres))
        BaseDeDatos.Commit(insert)

    def Enter():

        query= ("SELECT Idbus, Nombre, Numero_Plazas, Numero_Libres from Bus")
        buses=BaseDeDatos.Conexion(query)
        for bus in buses:
            idd= bus[0]
            nombre= bus[1]
            plazas= bus[2]
            libres= bus[3]
            nombre= Bus(idd, nombre, plazas, libres)
            AdminBus.listabus.append(nombre)

    def SelectBuses():
        query=("Select * from Bus")
        buses=BaseDeDatos.Conexion(query)
        return buses
        

    def UpdateNombre(bus,newnombre):
        
        old=bus.GetId()
        update= ("UPDATE BUS SET Nombre= '%s' where idbus= '%i'" %(newnombre,old))
        BaseDeDatos.Commit(update)

    def UpdatePlazasyLibres(bus):
        ID=int(bus.GetIdd())
        libres= int(bus.GetNumeroLibres())
        update=("UPDATE BUS SET Numero_libres= '%i' where idbus= '%i'" % (libres, ID))
        BaseDeDatos.Commit(update)
        


    def venda(bus,billetes):
        if billetes<0:
            raise Exception("El número introducido es negativo, por favor intente de nuevo")
        if billetes>bus.GetNumeroLibres():
            vendaok=False
            return vendaok

        else:
            libres=bus.GetNumeroLibres() - billetes
            bus.SetNumeroLibres(libres)
            vendaok=True
            return vendaok

        
            
            

    def devolucion(bus,billetes):
        
        if billetes<0:
            raise Exception("El número introducido es negativo, por favor intente de nuevo")
        if (bus.GetNumeroPlazas() - bus.GetNumeroLibres()) >= billetes:
            libres=bus.GetNumeroLibres()+billetes
            bus.SetNumeroLibres(libres)
            devolucionok=True

            return devolucionok
        
        else:
            devolucionok=False
            return devolucionok
        
            


