#from pasajero import Pasajero
import itertools


class Bus:
    newid=itertools.count()
    #listapasajero=[]

    def __init__(self,idd, nombre, numero_plazas, numero_libres):
        if numero_plazas<0:
            raise Exception("Ha ingresado un número negativo")
        self.SetNombre(nombre)
        self.SetNumeroPlazas(numero_plazas)
        self.SetNumeroLibres(numero_libres)
        self.SetIdd(idd)
        
    def SetIdd(self,idd):
        self.idd= idd 

    def SetNombre(self,nombre):
        self.nombre= nombre

    def SetNumeroPlazas(self,numero_plazas):
        self.numero_plazas=numero_plazas

    def SetNumeroLibres(self,numero_libres):
        self.numero_libres=numero_libres

    def GetIdd(self):
        return self.idd

    def GetNombre(self):
        return self.nombre

    def GetNumeroPlazas(self):
        return self.numero_plazas
    
    def GetNumeroLibres(self):
        return self.numero_libres
    
    def GetOcupados(self):
       return self.GetNumeroPlazas()- self.GetNumeroLibres()
    
    


## añadir en bus una lista de pasajeros vacia, añadir en la creación de buses y pasajeros:
        # bus1.listapasajero.append(nombrepasajero)
        #luego hacer algo para que esto lo cree en el momento de comprar billetes y que lo
        #borre en el caso que los billetes de ese bus sean 0. Osea sean todos devueltos.
        
    













